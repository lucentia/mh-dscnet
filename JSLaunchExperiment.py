import sys
import os
import mne
import time
import argparse
import ast
import traceback

import tensorflow as tf
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

from JSLib.JSLog import JSLog
from JSLib.JSADHDModels import MultiScaleCNN, EEGNet, JSModel_MLP_03, JSModel_Recurrent, VGGNet
from JSLib.JSMODMAModels import MultiScaleCNN_LSTM
from JSLib.CsvModelParams import CsvModelParams
from JSLib.JSModelCallback import BestAtMinLossWithAccThreshold


from asrpy import ASR

from sklearn.utils import shuffle
from scipy.io import loadmat

from mne_faster import (find_bad_channels, find_bad_epochs,
                        find_bad_components, find_bad_channels_in_epochs)

from pyriemann.clustering import Potato
import pyriemann

from autoreject import AutoReject

DATA_DIR="/data"
LOG_DIR="/logs"

def plot_mask(mask_test, mask_train, mask_val):
    
    plt.figure(1)
    plt.subplot(311)
    plt.plot(mask_test)
    plt.subplot(312)
    plt.plot(mask_train)
    plt.subplot(313)
    plt.plot(mask_val)
    plt.show()

def _load_montage(jsLog):
    
    montage_file_name = os.path.join(
        os.path.dirname(os.path.realpath(__file__)),
        'conf', 
        'Standard-10-20-Cap19new.loc')
    jsLog.w("LOADING MONTAGE [{}]".format(montage_file_name))
        
    return mne.channels.read_custom_montage(
            fname=montage_file_name,
            coord_frame='head')

def _create_eeg_info(jsLog, montage):

        eeg_info = mne.create_info(
            ch_names=montage.ch_names,
            ch_types=['eeg'] * len(montage.ch_names),
            sfreq=128)
        
        return eeg_info.set_montage(montage)

def _load_mat_data(jsLog, ix_subject, type_subject, id_type_subject, path):
    # Read the data
                    
    jsLog.w(f"[load_mat_data]: [{ix_subject}][{type_subject}][{id_type_subject}] - {path.name}",True)
    # ids_subjects.append(path.name[0:8])

    original_mat_data=loadmat(os.path.join(DATA_DIR, type_subject, path.name))

    for key in original_mat_data.keys():
        if key.startswith('v'):
            jsLog.w(key, True)
            np_original_mat_data=original_mat_data[key]
            jsLog.w(f"TYPE [{type(np_original_mat_data)}][{np_original_mat_data.shape}][{np_original_mat_data.max()}] - [{np_original_mat_data.min()}]", True)
    
    print(ix_subject)

    return np_original_mat_data

def _perform_pyriemann(jsLog, eeg_epochs):

    jsLog.w(f"PERFORMING PyRIEMANN", True)

    pyriemann_eeg_epochs=eeg_epochs.copy()
    # estimate the covariance matrices from our epochs
    covs = pyriemann.estimation.Covariances(estimator="lwf")
    cov_mats = covs.fit_transform(pyriemann_eeg_epochs.get_data())

    # Define the riemannian potato object. We use a threshold of 2 std's here.
    # With larger thresholds it might occur that no data is excluded at all
    potato = pyriemann.clustering.Potato(threshold=1.8)

    # Fit the Potato
    potato.fit(cov_mats)

    # Get the clean epoch indices and select only these for further processing
    clean_idx = potato.predict(cov_mats).astype(bool)
    return pyriemann_eeg_epochs[clean_idx]

def _perform_faster(jsLog, eeg_epochs):

     ### Step 1: mark bad channels
    eeg_epochs.info['bads'] = find_bad_channels(eeg_epochs, eeg_ref_corr=False)
    if len(eeg_epochs.info['bads']) > 0:
        eeg_epochs.interpolate_bads()


    ### Step 2: mark bad epochs
    bad_epochs = find_bad_epochs(eeg_epochs)
    if len(bad_epochs) > 0:
        eeg_epochs.drop(bad_epochs)

    ### Step 3: mark bad ICA components (using the build-in MNE functionality for this)
    # ica = mne.preprocessing.ICA(0.99).fit(eeg_epochs)
    # ica.exclude = find_bad_components(ica, eeg_epochs, use_metrics=['kurtosis', 'power_gradient', 'hurst', 'median_gradient'])
    # ica.apply(eeg_epochs)

    ### Step 4: mark bad channels for each epoch and interpolate them.
    bad_channels_per_epoch = find_bad_channels_in_epochs(eeg_epochs, eeg_ref_corr=False)
    for i, b in enumerate(bad_channels_per_epoch):
        if len(b) > 0:
            ep = eeg_epochs[i]
            ep.info['bads'] = b
            ep.interpolate_bads() 
            eeg_epochs._data[i, :, :] = ep._data[0, :, :]
        
    return eeg_epochs

def _filter_data(jsLog, original_mne_data, filter_params):

    jsLog.w(f"FILTERING PARAMS: {filter_params}", True)

    eeg_channels_data_filtered=original_mne_data.copy()

    eeg_channels_data_filtered.set_eeg_reference(ref_channels="average", projection=True)
    if filter_params[0] > 0: # High Pass
        jsLog.w(f"FILTERING HIGH: {filter_params[0]}")
        eeg_channels_data_filtered.filter(l_freq=filter_params[0], h_freq=None)
    
    if filter_params[1] > 0: # Low Pass   
        jsLog.w(f"FILTERING LOW: {filter_params[1]}") 
        eeg_channels_data_filtered.filter(l_freq=None, h_freq=filter_params[1])

    # spectrum_filtered = eeg_channels_data_filtered.compute_psd()
    # spectrum_filtered.plot(average=True, picks="data", exclude="bads")

    if filter_params[2] > 0: # Notch
        jsLog.w(f"FILTERING NOTCH: {filter_params[2]}")
        eeg_channels_data_filtered.notch_filter(filter_params[2], method='iir')
    
    
    if filter_params[3] != None and filter_params[3] > 0:
        jsLog.w(f"FILTERING EXTRA: {filter_params[3]}", True)
        eeg_channels_data_filtered.notch_filter(filter_params[3])
    else:
        jsLog.w(f"NOT FILTERING EXTRA: {filter_params[3]}", True)
    


    return eeg_channels_data_filtered
    
def _perform_asr(jsLog, original_mne_data):

    asr_completed=False
    tmp_mne_data=original_mne_data.copy()

    while not asr_completed:

        try:
            
            asr = ASR(sfreq=tmp_mne_data.info["sfreq"], cutoff=5)
            asr.fit(tmp_mne_data)
            raw_mne_asr = asr.transform(tmp_mne_data)
        except:
            # Remove one sample to fit with asr
            jsLog.w(f"Exception ASR. Lenght before [{tmp_mne_data.times.min()}][{tmp_mne_data.times.max()}]", True)
            tmp_mne_data=tmp_mne_data.crop(tmin=0.01)
            jsLog.w(f"Exception ASR. Lenght after [{tmp_mne_data.times.min()}][{tmp_mne_data.times.max()}]", True)
        else:
            jsLog.w(f"ASR OK", True)
            asr_completed=True

    return raw_mne_asr

def _perform_autoreject(jsLog, epochs):
    # suppress AR's output to save some space.
    # This works better in Colab than setting AR's verbosity level.

    # set parameters for autoreject
    n_interpolates = np.array([1, 4, 8])
    consensus_percs = np.linspace(0, 1.0, 11)
    ar = AutoReject(n_interpolates, consensus_percs, random_state=26042021)

    # fit the autoreject channel thresholds and bad epochs
    ar.fit(epochs)

    # transform the epochs, excluding/interpolating bad epochs
    epochs_ar, reject_log = ar.transform(epochs, return_log=True)

    return epochs_ar


def load_data(
        jsLog,
        outliers,
        preprocessing_type="faster", 
        duration=4.0, 
        overlap=2.0,
        perform_filter=True,
        filter_params=[1,60,50,0],
        perform_asr=False,
        perform_pyriemann=False,
        perform_faster=True,
        perform_autoreject=False,
        perform_csd=False
        ):

    jsLog.w(f"LOAD_DATA:\tFILTER [{perform_filter}] - PERFORM_ASR [{perform_asr}] - PERFORM_PYRIEMANN[{perform_pyriemann}] - PERFORM_FASTER[{perform_faster}] - PERFORM_AUTOREJECT[{perform_autoreject}] - PERFORM_CSD[{perform_csd}]", True)
    data_file_path=os.path.join(DATA_DIR, "adhd_full.npy")
    data_file_path_filtered=os.path.join(DATA_DIR, "adhd_full_filtered.npy")
    statistics_file_path=os.path.join(DATA_DIR, "adhd_stats.csv")

    montage=_load_montage(jsLog)
    eeg_info=_create_eeg_info(jsLog, montage)

    # ids_subjects=[]
    ix_subject=1
    sample_labels=[]
    subject_labels=[]

    sample_labels_filtered=[]
    subject_labels_filtered=[]


    data=[]
    statistics=[]

    model_data=None

    for type_subject, id_type_subject in zip(['CONTROL', 'ADHD'],[0,1]):
        for path in os.scandir(os.path.join(DATA_DIR, type_subject)):
            if path.is_file() and path.name.endswith(".mat"):
                if ix_subject not in outliers:

                    id_subject=path.name[:-4]
                    np_original_mat_data=_load_mat_data(jsLog, ix_subject, type_subject, id_type_subject, path)
                    original_mne_data = mne.io.RawArray(np_original_mat_data.T*10**-7, eeg_info)

                    raw_eeg_data_pipeline=original_mne_data.copy()
                    ## Filter Data
                    if perform_filter:
                        raw_eeg_data_pipeline=_filter_data(jsLog, raw_eeg_data_pipeline, filter_params=filter_params)
                    
                    ## ASR
                    if perform_asr:
                        raw_eeg_data_pipeline=_perform_asr(jsLog, raw_eeg_data_pipeline)


                    ## Convert to Epochs
                    new_events = mne.make_fixed_length_events(raw_eeg_data_pipeline, start=0, stop=None, duration=duration, overlap=overlap)

                    eeg_epochs = mne.Epochs(raw_eeg_data_pipeline, new_events, tmin=0, tmax=duration, baseline=None, preload=True)

                    jsLog.w(f"SHAPE BEFORE CLEAN EPOCHS: [{eeg_epochs.get_data().shape}]", True)
                    ## FASTER
                    if perform_faster:
                        eeg_epochs=_perform_faster(jsLog, eeg_epochs.copy())

                    ## AUTOREJECT
                    if perform_autoreject:
                        eeg_epochs=_perform_autoreject(jsLog, eeg_epochs.copy())

                    ## PYREIMANN
                    if perform_pyriemann:
                        eeg_epochs=_perform_pyriemann(jsLog, eeg_epochs.copy())

                    ## CSD
                    if perform_csd:
                        jsLog.w(f"COMPUTING CSD", True)
                        eeg_epochs=mne.preprocessing.compute_current_source_density(eeg_epochs)
                    
                    np_eeg_epochs=eeg_epochs.get_data()
                    jsLog.w(f"SHAPE AFTHER CLEAN EPOCHS: [{np_eeg_epochs.shape}]", True)

                    if ix_subject==1:
                        model_data = np_eeg_epochs
                    else:
                        model_data = np.vstack((model_data, np_eeg_epochs))

                    sample_labels+=[[ix_subject, id_subject, type_subject, id_type_subject, path.name]]*np_eeg_epochs.shape[0]
                    subject_labels+=[[ix_subject,id_type_subject]]
                    jsLog.w(f"SHAPE DATA: [{model_data.shape}] LABELS: [{len(sample_labels)}]", True)
                else:
                    jsLog.w(f"OUTLIER [{ix_subject}] {outliers}", True)

            ix_subject+=1

    sample_labels=np.asarray(sample_labels)
    subject_labels=np.asarray(subject_labels)
    
    return model_data, sample_labels, subject_labels

    
def get_train_val_test(jsLog, model_data, sample_labels, subject_labels, ix_test, num_validation_subjects, standardize=True, normalize=False, shuffle_var=False):

    unique_ix=subject_labels[:,0]

    mask_test=np.isin(sample_labels[:,0], ix_test)
    jsLog.w(f"MASK TEST SUM: [{mask_test.sum()}]", True)

    x_test=model_data[mask_test]
    y_test=sample_labels[mask_test,1]

    jsLog.w(f"X_TEST SHAPE [{x_test.shape}]", True)
    jsLog.w(f"Y_TEST SHAPE [{y_test.shape}] SUM: [{sum(y_test)}]", True)

    # The test subject is delete
    ix_train=np.delete(unique_ix, np.where(unique_ix == ix_test))
    # ix_train=np.delete(unique_ix, int(ix_test)-1, 0)
    jsLog.w(f"IX_TRAIN AFTER DELETE: TYPE IX_TRAIN[{type(ix_train)}] - TYPE UNIQUE_IX[{type(unique_ix)}]", True)
    jsLog.w(f"IX_TRAIN [{type(ix_train)}]")

    if num_validation_subjects > 0:
        ### First generate the two groups
        ix_train_MDD=subject_labels[np.isin(subject_labels[:,1], 1),0].copy()
        ix_train_HC =subject_labels[np.isin(subject_labels[:,1], 0),0].copy()

        jsLog.w(f"Before deleting test subject. NUM_VALIDATION_SUBJECTS [{num_validation_subjects}]", True)
        jsLog.w(f"TYPE ix_train_MDD: [{type(ix_train_MDD)}]", True)
        jsLog.w(ix_train_MDD, True)

        jsLog.w(f"TYPE ix_train_HC: [{type(ix_train_HC)}]", True)
        jsLog.w(ix_train_HC, True)

        ix_train_MDD=np.delete(ix_train_MDD, np.where(ix_train_MDD == ix_test))
        ix_train_HC=np.delete(ix_train_HC, np.where(ix_train_HC == ix_test))

        jsLog.w("After deleting test subject [{num_validation_subjects}]", True)
        jsLog.w(ix_train_MDD)
        jsLog.w(ix_train_HC)

        ### Second a num_validation_subjects//2 subject of each group is randomly selected
        ix_val=np.concatenate((
            np.random.choice(ix_train_MDD, size=num_validation_subjects//2, replace=False), 
            np.random.choice(ix_train_HC,  size=num_validation_subjects//2, replace=False)
        ))

        ### Finally we delete this tho values from ix_train
        ix_train=np.delete(ix_train, np.isin(ix_train, ix_val))

        ### Mask for val
        mask_val=np.isin(sample_labels[:,0], ix_val)

        x_val=model_data[mask_val]
        y_val=sample_labels[mask_val,1]
    else:
        jsLog.w("NO VALIDATION DATA", True)
        x_val=None
        y_val=None


    ### Mask for train
    mask_train=np.isin(sample_labels[:,0], ix_train)

    x_train=model_data[mask_train]
    y_train=sample_labels[mask_train,1]

   
    jsLog.w(f"TRAIN:\t T[{type(ix_train)}] [{ix_train.shape}]\n {ix_train}", True)
    if num_validation_subjects > 0: jsLog.w(f"VAL:  \t T[{type(ix_val)}][{ix_val.shape}]\n {ix_val}", True)
    jsLog.w(f"TEST: \t T[{type(ix_test)}][{ix_test}]\n {ix_test}", True)

    if num_validation_subjects > 0:
        jsLog.w(f"LEN SAMPLE_LABELS [{len(sample_labels)}] - LEN SUM [{len(x_train)+len(x_val)+len(x_test)}][{len(y_train)+len(y_val)+len(y_test)}]")
    else:
        jsLog.w(f"LEN SAMPLE_LABELS_NO_VAL [{len(sample_labels)}] - LEN_SUM_NO_VAL [{len(x_train)+len(x_test)}][{len(y_train)+len(y_test)}]")

    # plot_mask(mask_test, mask_train, mask_val)

    jsLog.w("Statistics Before Norm:")
    
    jsLog.w(f"ALL DATA: [{model_data.shape}][{model_data.max()}] - [{model_data.min()}] - [{model_data.mean()}] - [{model_data.std()}]")
    jsLog.w(f"TRAIN: [{x_train.shape}][{x_train.max()}] - [{x_train.min()}] - [{x_train.mean()}] - [{x_train.std()}]")
    if num_validation_subjects > 0: jsLog.w(f"VAL: [{x_val.shape}][{x_val.max()}] - [{x_val.min()}] - [{x_val.mean()}] - [{x_val.std()}]")
    jsLog.w(f"TEST: [{x_test.shape}][{x_test.max()}] - [{x_test.min()}] - [{x_test.mean()}] - [{x_test.std()}]")

    if standardize: # z-score normalization
        mean_x = x_train.mean()
        std_x = x_train.std()

        x_train=(x_train-mean_x)/std_x
        if num_validation_subjects > 0: x_val=(x_val-mean_x)/std_x
        x_test=(x_test-mean_x)/std_x
    
    ## Normalization
    if normalize: # min_max normalization using only train data
        min_x = x_train.min()
        max_x = x_train.max()

        x_train=(x_train-min_x)/(max_x-min_x)
        if num_validation_subjects > 0: x_val=(x_val-min_x)/(max_x-min_x)
        x_test=(x_test-min_x)/(max_x-min_x)

    
    if shuffle_var:
        jsLog.w(f"SHUFFLE OK", True)
        x_train, y_train = shuffle(x_train, y_train)
        if num_validation_subjects > 0: x_val, y_val = shuffle(x_val, y_val)
        x_test, y_test = shuffle(x_test, y_test)

    jsLog.w("Statistics After Norm:")
    
    jsLog.w(f"ALL DATA: [{model_data.shape}][{model_data.max()}] - [{model_data.min()}] - [{model_data.mean()}] - [{model_data.std()}]", True)
    jsLog.w(f"TRAIN: [{x_train.shape}][{x_train.max()}] - [{x_train.min()}] - [{x_train.mean()}] - [{x_train.std()}]", True)
    if num_validation_subjects > 0: jsLog.w(f"VAL: [{x_val.shape}][{x_val.max()}] - [{x_val.min()}] - [{x_val.mean()}] - [{x_val.std()}]", True)
    jsLog.w(f"TEST: [{x_test.shape}][{x_test.max()}] - [{x_test.min()}] - [{x_test.mean()}] - [{x_test.std()}]", True)

    if num_validation_subjects > 0: 
        jsLog.w(f"LEN SAMPLE_LABELS [{len(sample_labels)}] - LEN_SUM [{len(x_train)+len(x_val)+len(x_test)}][{len(y_train)+len(y_val)+len(y_test)}]")
    else:
        jsLog.w(f"LEN SAMPLE_LABELS_NO_VAL [{len(sample_labels)}] - LEN_SUM_NO_VAL [{len(x_train)+len(x_test)}][{len(y_train)+len(y_test)}]")

    return x_train, y_train, x_val, y_val, x_test, y_test

def _get_optimizer(jsLog, adam_parameters):
    
    jsLog.w(f"ADAM PARAMETERS [{adam_parameters}]")
    optimizer=tf.keras.optimizers.Adam()

    if adam_parameters != None:
        if adam_parameters["learning_rate"] == "ExponentialDecay":
            lr_schedule = tf.keras.optimizers.schedules.ExponentialDecay(
                initial_learning_rate=1e-4,
                decay_steps=10000,
                decay_rate=0.9)
        else:
            lr_schedule=adam_parameters["learning_rate"]

        optimizer=tf.keras.optimizers.Adam(
            learning_rate=lr_schedule,
            beta_1=adam_parameters["beta_1"],
            beta_2=adam_parameters["beta_2"],
            use_ema=adam_parameters["use_ema"]
        )

    return optimizer
def _get_callbacks(jsLog, callback_list_str):

    jsLog.w(f"GET_CALLBACKS: {callback_list_str}", True)
    callbacks=None

    if callback_list_str != "None":
        callbacks=[]
        for callback in ast.literal_eval(callback_list_str):
            if callback=="EarlyStoppingAtMinLossWithThreshold":
                callbacks.append(BestAtMinLossWithAccThreshold(jsLog, 0.96, 5))

    return callbacks
def _get_subjects_ids(jsLog, subject_labels, outliers):

    return subject_labels[np.isin(subject_labels[:,0], outliers, invert=True),0]

def _show_layers(m):
    index_layer=0
    for layer in m.layers:
        jsLog.w(f"Name [{layer.name}]", True)       

        index_layer+=1

if __name__ == "__main__":

    np.random.seed(333)

    parser = argparse.ArgumentParser(
        prog = 'Adhd',
        description = 'Adhd',
        epilog = 'Adhd')

    parser.add_argument('-i', '--id')
    parser.add_argument('-m', '--model')
    args = parser.parse_args()
                        
    id_configuration=int(args.id)
    model_name=args.model

    jsLog=JSLog(f"{model_name}_ID_{id_configuration}")

    #p = CsvModelParams("MultiScaleCNN", id_configuration)
    p = CsvModelParams(model_name, id_configuration)

    duration=float(p.parameters["window_size"])
    overlap=float(p.parameters["overlapping"])*duration
    shuffle_var=bool(int(p.parameters["shuffle"])) if "shuffle" in p.parameters else False

    

    adam_parameters=ast.literal_eval(p.parameters["adam_parameters"])
    num_validation_subjects=int(p.parameters["num_validation_subjects"])

    print(adam_parameters)

    perform_filter=bool(int(p.parameters["perform_filter"]))
    filter_params=ast.literal_eval(p.parameters["filter_params"])
    perform_asr=bool(int(p.parameters["perform_asr"]))
    perform_pyriemann=bool(int(p.parameters["perform_pyriemann"]))
    perform_faster=bool(int(p.parameters["perform_faster"]))
    perform_autoreject=bool(int(p.parameters["perform_autoreject"]))
    perform_csd=bool(int(p.parameters["perform_csd"]))

    standardize=bool(int(p.parameters["standardize"]))
    normalize=bool(int(p.parameters["normalize"]))

    outliers=[0] if p.parameters["outliers"]=="None" else ast.literal_eval(p.parameters["outliers"])
    jsLog.w(f"OUTLIERS [{type(outliers)}] [{outliers}]", True)

    jsLog.w(f"DURATION [{duration}] OVERLAP [{overlap}]", True)

    jsLog.w(f"FILTER [{perform_filter}][{p.parameters['perform_filter']}] - \
              PERFORM_ASR [{perform_asr}][{p.parameters['perform_asr']}] - \
              PERFORM_PYRIEMANN[{perform_pyriemann}][{p.parameters['perform_pyriemann']}] - \
              PERFORM_FASTER[{perform_faster}][{p.parameters['perform_faster']}] - \
              PERFORM_AUTOREJECT[{perform_autoreject}][{p.parameters['perform_autoreject']}] - \
              PERFORM_CSD[{perform_csd}][{p.parameters['perform_csd']}]", True)

    model_data, sample_labels, subject_labels = load_data(
        jsLog,
        outliers,
        p.parameters["preprocessing_type"], 
        duration, 
        overlap, 
        perform_filter,
        filter_params,
        perform_asr,
        perform_pyriemann,
        perform_faster,
        perform_autoreject,
        perform_csd)

    print(f"SHAPES: [{model_data.shape}][{len(sample_labels)}]")

    sample_labels_st=sample_labels[:,[0,3]].astype(np.int64)

    print(f"Type SAMPLE_LABELS: {type(sample_labels_st[0,0])}")
    print(f"Type SUBJECT_LABELS: {type(subject_labels[0,0])}")

    print(sample_labels_st)
    
    metrics=[
        tf.keras.metrics.BinaryAccuracy(name="binary_accuracy"),
        tf.keras.metrics.Precision(name="precision"),
        tf.keras.metrics.Recall(name="recall"),
        tf.keras.metrics.TruePositives(name="true_positives"),
        tf.keras.metrics.TrueNegatives(name="true_negatives"),
        tf.keras.metrics.FalsePositives(name="false_positives"),
        tf.keras.metrics.FalseNegatives(name="false_negatives")
    ]

    callbacks=_get_callbacks(jsLog, p.parameters["callback_list"])
    
    # subject_labels=_remove_outliers(jsLog, subject_labels, outliers)

    try:
        for num_iter in range(1, int(p.parameters["num_iter"])+1):
            # subject_labels(ix_subject, id_type_subject)
            num_fold=1

            ## Removing outliers from subjects
            for subject_label in subject_labels:
                
                index_subject=subject_label[0]
                type_subject=subject_label[1]

                start_time = time.time()
                print(f"{type(index_subject)} {index_subject}")
                x_train, y_train, x_val, y_val, x_test, y_test = get_train_val_test(jsLog, model_data, sample_labels_st, subject_labels, index_subject, num_validation_subjects, standardize, normalize, shuffle_var=shuffle_var)

                if model_name=="EEGNet":
                    jsModel=EEGNet(p)
                elif model_name=="JSModel_MLP_03":
                    jsModel=JSModel_MLP_03(p)
                elif model_name=="JSModel_Recurrent":
                    jsModel=JSModel_Recurrent(p)
                elif model_name=="VGGNet":
                    jsModel=VGGNet(p)
                else:
                    jsModel=MultiScaleCNN(p)
                
                jsModel.compile(
                    optimizer=_get_optimizer(jsLog, adam_parameters),
                    loss=tf.keras.losses.BinaryCrossentropy(),
                    metrics=metrics
                )

                start_fit = time.time()
                history = jsModel.fit(
                    x_train, 
                    y_train, 
                    batch_size=int(p.parameters["batch_size"]), 
                    epochs=int(p.parameters["num_epochs"]), 
                    validation_data=(x_val, y_val) if num_validation_subjects > 0 else None,
                    callbacks=callbacks,
                    verbose=2
                )
                end_fit = time.time()

                jsModel.summary()

                results = jsModel.evaluate(x_test, y_test, batch_size=int(p.parameters["batch_size"]), verbose=2)
                

                jsLog.save_history(
                    p.get_params_name(), 
                    p.get_params_values(end_fit - start_fit,num_iter, num_fold), 
                    history)

                jsLog.save_results(
                    p.get_params_name(),
                    p.get_params_values(end_fit - start_fit,num_iter, num_fold),
                    results)
                
                predictions_subject = jsModel.predict(x_test, batch_size=int(p.parameters["batch_size"]))
                jsLog.save_results_by_subject_type(
                    p.get_params_name(),
                    p.get_params_values(0, num_iter, num_fold),
                    predictions_subject,
                    index_subject,
                    type_subject
                )

                _show_layers(jsModel)

                
                #hidden_model=tf.keras.models.Model(inputs=jsModel.input, outputs=jsModel.get_layer(name='Concat_Blocks_Maximum').output)
                #partial_output=hidden_model.predict(x_test, batch_size=int(p.parameters["batch_size"]))
                #
                #
                #jsLog.w(f"PARTIAL_OUTPUT [{partial_output.shape}]", True)
                
                end_time = time.time()

                num_fold+=1
    
    except Exception as e:
        jsLog.w(f"EXCEPTION: [{e}]", True)
        traceback.print_exc()

        
    

   
 
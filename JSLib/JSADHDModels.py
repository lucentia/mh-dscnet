import ast
import tensorflow as tf




# MultiScaleCNN_ParallelBlock
class Classification_DenseParallel_Block(tf.keras.layers.Layer):
    def __init__(self,units:int,norm_rate:float,activation:str):

        super(Classification_DenseParallel_Block, self).__init__()

        self.block_classification_dense=tf.keras.layers.Dense(
            units, 
            kernel_constraint=tf.keras.constraints.max_norm(norm_rate))
        self.block_activation=tf.keras.layers.Activation(activation)

    def call(self, input_tensor, training=False):

        x=self.block_classification_dense(input_tensor)
        x=self.block_activation(x)

        return x

class Classification_DenseParallel(tf.keras.layers.Layer):
    def __init__(self,p):

        self._init_params(p)

        super(Classification_DenseParallel, self).__init__()

        self.class_block_01=Classification_DenseParallel_Block(2, self.norm_rate, 'gelu')
        self.class_block_02=Classification_DenseParallel_Block(2, self.norm_rate, 'tanh')
        self.class_block_03=Classification_DenseParallel_Block(4, self.norm_rate, 'gelu')
        self.class_block_04=Classification_DenseParallel_Block(4, self.norm_rate, 'tanh')
        self.class_block_05=Classification_DenseParallel_Block(8, self.norm_rate, 'gelu')
        self.class_block_06=Classification_DenseParallel_Block(8, self.norm_rate, 'tanh')

        self.concat=tf.keras.layers.Concatenate()
        self.drop01=tf.keras.layers.Dropout(self.dropout_rate)

        self.classification_concat=Classification_DenseParallel_Block(32, self.norm_rate, 'gelu')
        self.drop02=tf.keras.layers.Dropout(self.dropout_rate)

        self.classification_output=Classification_Dense(p)

    def call(self, input_tensor, training=False):

        x1=self.class_block_01(input_tensor)
        x2=self.class_block_02(input_tensor)
        x3=self.class_block_03(input_tensor)
        x4=self.class_block_04(input_tensor)
        x5=self.class_block_06(input_tensor)
        x6=self.class_block_06(input_tensor)

        x=self.concat([x1, x2, x3, x4, x5, x6])
        x=self.drop01(x)

        x=self.classification_concat(x)
        x=self.drop02(x)

        output=self.classification_output(x)

        return output

    def _init_params(self, p):

        self.p=p
        self.norm_rate=float(p.parameters["norm_rate"])
        self.dropout_rate=float(p.parameters["dropout_rate"])
        
    
class Classification_Dense(tf.keras.layers.Layer):
    def __init__(self,p):

        super(Classification_Dense, self).__init__()

        self._init_params(p)

        self.block_classification_dense=tf.keras.layers.Dense(
            1, 
            kernel_constraint = tf.keras.constraints.max_norm(self.norm_rate))
        self.block_classification_activation=tf.keras.layers.Activation('sigmoid', name = 'sigmoid')

    def call(self, input_tensor, training=False):

        classification=self.block_classification_dense(input_tensor)
        output=self.block_classification_activation(classification)

        return output

    def _init_params(self, p):

        self.p=p
        self.norm_rate=float(p.parameters["norm_rate"])
        

class MultiScaleCNN_ParallelBlock(tf.keras.Model):
  
    def __init__(self,p):

        super(MultiScaleCNN_ParallelBlock, self).__init__()

        self.int_seed=int(p.parameters["int_seed"])
        self.input_gaussian_stddev=float(p.parameters["input_gaussian_stddev"])
        self.num_input_conv_layers=int(p.parameters["num_input_conv_layers"])
        filters_f1=int(p.parameters["filters_f1"])
        same_number_filters=p.parameters["same_number_filters"]=='True'
        kernel_size=int(p.parameters["kernel_size"])
        self.strides_conv1d=int(p.parameters["strides_conv1d"])
        self.dilation_rate_conv1d=int(p.parameters["dilation_rate_conv1d"])
        self.block_01_gaussian_stddev=float(p.parameters["block_01_gaussian_stddev"])
        activation_block_01=p.parameters["activation_block_01"]
        pool_type_block_01=p.parameters["pool_type_block_01"]
        pool_size_block_01=int(p.parameters["pool_size_block_01"])
        padding=p.parameters["padding"]

        depth_kernel_size=int(p.parameters["depth_kernel_size"])
        depth_strides=int(p.parameters["depth_strides"])
        depth_padding=p.parameters["depth_padding"]
        depth_multiplier=int(p.parameters["depth_multiplier"])
        depth_use_bias=p.parameters["depth_use_bias"]=='True'
        depth_max_norm=float(p.parameters["depth_max_norm"])

        use_bias=p.parameters["depth_max_norm"]=='True'

        self.num_input_sepconv_layers=int(p.parameters["num_input_sepconv_layers"])
        filters_f2=int(p.parameters["filters_f2"])
        kernel_size_separable_conv=int(p.parameters["kernel_size_separable_conv"])
        activation_block_02=p.parameters["activation_block_02"]
        pool_type_block_02=p.parameters["pool_type_block_02"]
        pool_size_block_02=int(p.parameters["pool_size_block_02"])
        dropout_rate=float(p.parameters["dropout_rate"])
        norm_rate=float(p.parameters["norm_rate"])
        apply_batch_norm=p.parameters["apply_batch_norm"]=='True'
        classification_type=p.parameters["classification_type"]

        #print("ACT_01", activation_block_01)
        #print("ACT_02", activation_block_02)

        self.p=p
        self.permute = tf.keras.layers.Permute((2,1))

        if self.input_gaussian_stddev > 0:
            self.input_gaussian_norm=tf.keras.layers.GaussianNoise(
                self.input_gaussian_stddev, 
                seed=self.int_seed)

        # Block 01
        
        self.block_01_conv1D_layers=[]
        # self.block_01_gaussian_layers=[]
        for index_input_conv_layers in range(self.num_input_conv_layers):

            if same_number_filters: 
                filter_multiplier=1
            else:
                filter_multiplier=2**index_input_conv_layers

            self.block_01_conv1D_layers.append(
                tf.keras.layers.Conv1D(
                    filters_f1*filter_multiplier, 
                    kernel_size//2**index_input_conv_layers,
                    strides=self.strides_conv1d,
                    padding=padding,
                    dilation_rate=self.dilation_rate_conv1d,
                    use_bias=use_bias)
            )
        
        if self.block_01_gaussian_stddev > 0:
            self.block_01_gaussian_layer=tf.keras.layers.GaussianNoise(
                    self.block_01_gaussian_stddev, 
                    seed=self.int_seed)
            
        
        self.block_01_norm_01=tf.keras.layers.BatchNormalization()

        self.block_01_deptwise1D=tf.keras.layers.DepthwiseConv1D(
            depth_kernel_size, 
            strides=depth_strides,
            padding=depth_padding,
            depth_multiplier=depth_multiplier,
            use_bias=depth_use_bias,
            depthwise_constraint = tf.keras.constraints.max_norm(depth_max_norm))
        
        self.block_01_norm_02=tf.keras.layers.BatchNormalization()
        self.block_01_activation=tf.keras.layers.Activation(activation_block_01)
        if pool_type_block_01=='MAX':
            # print("###### MAX")
            self.block_01_pool=tf.keras.layers.MaxPooling1D(pool_size_block_01)
        else:
            self.block_01_pool=tf.keras.layers.AveragePooling1D(pool_size_block_01)
        
        self.block_01_drop=tf.keras.layers.Dropout(dropout_rate)

        # Block 02

        self.block_02_sepConv1D_layers=[]
        for index_input_sepConv1D_layers in range(self.num_input_sepconv_layers):
            self.block_02_sepConv1D_layers.append(
                tf.keras.layers.SeparableConv1D(
                    filters_f2*2**index_input_sepConv1D_layers, 
                    kernel_size_separable_conv//2**index_input_sepConv1D_layers,
                    use_bias=use_bias, 
                    padding=padding)
            )
        
        self.block_02_norm_01=tf.keras.layers.BatchNormalization()
        self.block_02_activation_01=tf.keras.layers.Activation(activation_block_02)
        if pool_type_block_02=='MAX':
            # print("###### MAX 02")
            self.block_02_pool=tf.keras.layers.MaxPooling1D(pool_size_block_02)
        else:
            self.block_02_pool=tf.keras.layers.AveragePooling1D(pool_size_block_02)

        self.block_02_drop=tf.keras.layers.Dropout(dropout_rate)
        self.block_02_flat=tf.keras.layers.Flatten(name = 'flatten')

        # Classification

        if classification_type=='dense':
            self.block_classification=Classification_Dense(p)
        elif classification_type=='dense_parallel':
            self.block_classification=Classification_DenseParallel(p)

    def call(self, input_tensor, training=False):

        input=self.permute(input_tensor)
        if self.input_gaussian_stddev > 0:
            input=self.input_gaussian_norm(input, training=training)
            
        # BLOCK 01
        x_block_01_conv=[]
        for block_01_conv1D_layer in self.block_01_conv1D_layers:
            x_block_01_conv.append(
                block_01_conv1D_layer(input)
            )

        block_01=tf.concat(x_block_01_conv, -1)
        if self.block_01_gaussian_stddev > 0:
            block_01=self.block_01_gaussian_layer(block_01, training=training)
        
        if self.p.parameters["apply_batch_norm"]=='True':
            block_01=self.block_01_norm_01(block_01, training=training)
        block_01=self.block_01_deptwise1D(block_01)
        
        if self.p.parameters["apply_batch_norm"]=='True':
            block_01=self.block_01_norm_02(block_01, training=training)
        block_01=self.block_01_activation(block_01)
        block_01=self.block_01_pool(block_01)
        block_01=self.block_01_drop(block_01)

        # BLOCK 02
        x_block_02_sepConv=[]
        for block_02_sepConv1D in self.block_02_sepConv1D_layers:
            x_block_02_sepConv.append(
                block_02_sepConv1D(block_01)
            )
        block_02=tf.concat(x_block_02_sepConv, -1)

        if self.p.parameters["apply_batch_norm"]=='True':
            block_02=self.block_02_norm_01(block_02, training=training)
        
        block_02=self.block_02_activation_01(block_02)
        block_02=self.block_02_pool(block_02)
        block_02=self.block_02_drop(block_02)
        block_02=self.block_02_flat(block_02)

        # Classification
        output=self.block_classification(block_02)

        return output

    def model(self, shape=(19, 256)):
        x = tf.keras.Input(shape=shape)
        return tf.keras.Model(inputs=[x], outputs=self.call(x))
 
class MHSCLNetBlock(tf.keras.layers.Layer):
  
    def __init__(self,
    filters:int=64, 
    kernel_size:int=64,
    num_convolutional_layers:int=2,
    activation_conv_list:list=[],
    batch_normalization:bool=False,
    activation_dense:str='relu', 
    padding:str='same',
    depth_multiplier:int=1,
    depthwise_constraint=None,
    depthwise_regularizer=None,
    use_bias:bool=True,
    partial_output:int=19,
    pool_size:int=2,
    pool_type:str="AVG",
    dropout_rate:float=0.5,
    block_name:str="MHSCLNetBlock",
    p=None):

        super(MHSCLNetBlock, self).__init__()

        self._set_parameters(p)

        self.batch_normalization=batch_normalization

        self.conv_layers=[]
        self.norm_layers=[]
        for num_convolutional_layer, activation_conv in zip(range(num_convolutional_layers), activation_conv_list):
            if(batch_normalization):
                self.norm_layers.append(tf.keras.layers.BatchNormalization())
                activation_conv=None

            if p.parameters["same_filters"]=='0':
                filters=filters//2
                print(f"FILTERS [{filters}] - NUM_CONV[{num_convolutional_layer+1}]")

            self.conv_layers.append(
                
                tf.keras.layers.SeparableConv1D(
                    filters=filters, 
                    kernel_size=kernel_size, 
                    padding=padding,
                    depth_multiplier=depth_multiplier,
                    depthwise_constraint=depthwise_constraint,
                    depthwise_regularizer=depthwise_regularizer,
                    activation=activation_conv,
                    use_bias=use_bias,
                    name=f"{block_name}_{num_convolutional_layer:02}"
                )
            )
                
        if(batch_normalization):
            self.conv_activation=tf.keras.layers.Activation('relu')

        if(pool_type=="AVG"):
            self.pool = tf.keras.layers.AveragePooling1D(
                pool_size=pool_size, 
                name=block_name + '_AvgPool')
        elif(pool_type=="MAX"):
            self.pool = tf.keras.layers.MaxPool1D(
                pool_size=pool_size, 
                name=block_name + '_MaxPool')


        self.flat = tf.keras.layers.Flatten(
            name=block_name + '_Flat')

        self.partial_output = tf.keras.layers.Dense(
            partial_output, 
            activation=activation_dense, 
            use_bias=use_bias,
            name=block_name + '_PartialClassifier')

        if self.training_dropout <= 1:
            self.dropout = tf.keras.layers.Dropout(
                dropout_rate, 
                name=block_name + '_Dropout')

    def call(self, input, training=False):
        
        x=input

        if not self.batch_normalization:
            for conv_layer in self.conv_layers:
                x=conv_layer(x)
        else:
            for conv_layer, batch_norm_layer in zip(self.conv_layers, self.norm_layers):
                x=conv_layer(x)
                x=batch_norm_layer(x, training=training)
                
            x=self.conv_activation(x)
            
        x=self.pool(x)
        x=self.flat(x)
        x=self.partial_output(x)
        
        if self.training_dropout <= 1:
            x=self.dropout(x)

        return x
    
    def _set_parameters(self, p):
        self.p=p

        self.training_dropout=int(p.parameters["training_dropout"])

class MultiScaleCNN_Classification_Dense(tf.keras.layers.Layer):
    def __init__(self,p):

        super(MultiScaleCNN_Classification_Dense, self).__init__()

        self._init_params(p)

        self.block_classification_dense=tf.keras.layers.Dense(
            1, 
            kernel_constraint=self.dense_kernel_constraint,
            kernel_regularizer=self.conv_kernel_regularizer,
            )
        self.block_classification_activation=tf.keras.layers.Activation('sigmoid', name = 'sigmoid')

    def call(self, input_tensor, training=False):

        classification=self.block_classification_dense(input_tensor)
        output=self.block_classification_activation(classification)

        return output

    def _init_params(self, p):

        self.p=p
        
        self.dense_kernel_constraint=None
        if p.parameters["dense_kernel_constraint"]=='None':
            self.dense_kernel_constraint=None
        else:
           self.dense_kernel_constraint=tf.keras.constraints.max_norm(
                float(p.parameters["dense_kernel_constraint"]))

        self.conv_kernel_regularizer=None
        if p.parameters["dense_kernel_regularizer"]=='None':
                self.conv_kernel_regularizer=None
        else:
            self.conv_kernel_regularizer=tf.keras.regularizers.L2(
                l2=float(p.parameters["dense_kernel_regularizer"])
            )

        self.dropout_rate=float(p.parameters["dropout_rate"]) if "dropout_rate" in p.parameters["dropout_rate"] else 0.25
            

class MultiScaleCNN_Classification_Deep_Dense(tf.keras.layers.Layer):
    def __init__(self,p):

        super(MultiScaleCNN_Classification_Deep_Dense, self).__init__()

        self._init_params(p)

        self.block_classification_dense_01=tf.keras.layers.Dense(
            128, 
            activation='relu',
            kernel_constraint=self.dense_kernel_constraint,
            kernel_regularizer=self.conv_kernel_regularizer,
            )
        self.drop_01=tf.keras.layers.Dropout(self.dropout_rate)

        self.block_classification_dense_02=tf.keras.layers.Dense(
            64,
            activation='relu',
            kernel_constraint=self.dense_kernel_constraint,
            kernel_regularizer=self.conv_kernel_regularizer,
            )
        self.drop_02=tf.keras.layers.Dropout(self.dropout_rate)

        self.block_classification_dense_03=tf.keras.layers.Dense(
            32,
            activation='relu',
            kernel_constraint=self.dense_kernel_constraint,
            kernel_regularizer=self.conv_kernel_regularizer,
            )
        self.drop_03=tf.keras.layers.Dropout(self.dropout_rate)

        self.block_classification_dense=tf.keras.layers.Dense(
            1, 
            kernel_constraint=self.dense_kernel_constraint,
            kernel_regularizer=self.conv_kernel_regularizer,
            )
        self.block_classification_activation=tf.keras.layers.Activation('sigmoid', name = 'sigmoid')

    def call(self, input_tensor, training=False):

        x_01=self.block_classification_dense_01(input_tensor)
        x_01=self.drop_01(x_01, training=training)

        x_02=self.block_classification_dense_02(x_01)
        x_02=self.drop_02(x_02, training=training)

        x_03=self.block_classification_dense_03(x_02)
        x_03=self.drop_03(x_03, training=training)

        classification=self.block_classification_dense(x_03)
        output=self.block_classification_activation(classification)

        return output

    def _init_params(self, p):

        self.p=p
        
        self.dense_kernel_constraint=None
        if p.parameters["dense_kernel_constraint"]=='None':
            self.dense_kernel_constraint=None
        else:
           self.dense_kernel_constraint=tf.keras.constraints.max_norm(
                float(p.parameters["dense_kernel_constraint"]))

        self.conv_kernel_regularizer=None
        if p.parameters["dense_kernel_regularizer"]=='None':
                self.conv_kernel_regularizer=None
        else:
            self.conv_kernel_regularizer=tf.keras.regularizers.L2(
                l2=float(p.parameters["dense_kernel_regularizer"])
            )

        self.dropout_rate=float(p.parameters["dropout_rate"]) if "dropout_rate" in p.parameters["dropout_rate"] else 0.25

class MultiScaleCNN(tf.keras.Model):

    def __init__(self, p):
        super(MultiScaleCNN, self).__init__()

        self._set_parameters(p)
        
        self.permute = tf.keras.layers.Permute((2,1))

        if p.parameters["previous_conv"]=='1':
            self.previous_conv=tf.keras.layers.Conv1D(
                64,
                64,
                padding='same'
            )
            
        self.block_list=[]
        for num_block in range(int(p.parameters["num_pb"])):
            div=2**num_block
            
            # use_bias_test=bool(int(p.parameters["use_bias"]))
            # depth_multiplier_test=int(p.parameters["depth_multiplier"])
            # print(f"Use Bias[{use_bias_test}] - depth_multiplier[{depth_multiplier_test}] - [{div}]")
            
            if p.parameters["conv_depthwise_constraint"]=='None':
                conv_depthwise_constraint=None
            else:
                conv_depthwise_constraint=tf.keras.constraints.max_norm(
                    float(p.parameters["conv_depthwise_constraint"])
                )

            if p.parameters["conv_depthwise_regularizer"]=='None':
                conv_depthwise_regularizer=None
            else:
                conv_depthwise_regularizer=tf.keras.regularizers.L2(
                    l2=float(p.parameters["conv_depthwise_regularizer"])
                )

            # print(conv_depthwise_regularizer)

            self.block_list.append( 
                MHSCLNetBlock(
                    filters=int(p.parameters["filters"]), 
                    kernel_size=int(p.parameters["kernel_size"])//div,
                    num_convolutional_layers=int(p.parameters["num_convolutional_layers"]),
                    activation_conv_list=ast.literal_eval(p.parameters["activation_conv_list"]),
                    batch_normalization=(p.parameters["batch_normalization"]=='1'),
                    activation_dense=p.parameters["activation_dense"], 
                    padding=p.parameters["padding"],
                    depth_multiplier=int(p.parameters["depth_multiplier"]),
                    depthwise_constraint=conv_depthwise_constraint,
                    depthwise_regularizer=conv_depthwise_regularizer,
                    use_bias=bool(int(p.parameters["use_bias"])),
                    partial_output=int(p.parameters["partial_output"]),
                    pool_size=int(p.parameters["pool_size"]),
                    pool_type=p.parameters["pool_type"],
                    dropout_rate=float(p.parameters["dropout_rate"]),
                    block_name=f"PB_{num_block:02}",
                    p=self.p
                )
            )

        if p.parameters["concat_mode"]=="AVG":
            self.concatBlock = tf.keras.layers.Average(name='Concat_Blocks_Average')
        elif p.parameters["concat_mode"]=="MAX":
            self.concatBlock = tf.keras.layers.Maximum(name='Concat_Blocks_Maximum')
        elif p.parameters["concat_mode"]=="MIN":
            self.concatBlock = tf.keras.layers.Minimum(name='Concat_Blocks_Minimum')
        elif p.parameters["concat_mode"]=="ALL":
            self.concatBlock_avg=tf.keras.layers.Average(name='Concat_Blocks_Average')
            self.concatBlock_max=tf.keras.layers.Maximum(name='Concat_Blocks_Maximum')
            self.concatBlock_min=tf.keras.layers.Minimum(name='Concat_Blocks_Minimum')
            self.concatBlock=tf.keras.layers.Concatenate(name='Concat_Blocks_Concatenate')
        else:
            self.concatBlock=tf.keras.layers.Concatenate(name='Concat_Blocks_Concatenate')

        if p.parameters["attention"]=="1":
            self.attention=tf.keras.layers.Dense(
                int(p.parameters["attention_units"]), 
                activation=p.parameters["attention_activation"],
                use_bias=bool(int(p.parameters["use_bias"])),
                name='Attention')

        self.drop = tf.keras.layers.Dropout(float(p.parameters["dropout_rate"]))

        if p.parameters["dense_kernel_constraint"]=='None':
            dense_kernel_constraint=None
        else:
            dense_kernel_constraint=tf.keras.constraints.max_norm(
                float(p.parameters["dense_kernel_constraint"]))

        if p.parameters["dense_kernel_regularizer"]=='None':
                conv_kernel_regularizer=None
        else:
            conv_kernel_regularizer=tf.keras.regularizers.L2(
                l2=float(p.parameters["dense_kernel_regularizer"])
            )

        # print(conv_kernel_regularizer)

        if self.classifier==0:
            self.block_classification = tf.keras.layers.Dense(
            1, 
            use_bias=bool(int(p.parameters["use_bias"])),
            kernel_constraint=dense_kernel_constraint,
            kernel_regularizer=conv_kernel_regularizer,
            )
            self.final_activation=tf.keras.layers.Activation('sigmoid')
        elif self.classifier==1:
            self.block_classification=MultiScaleCNN_Classification_Dense(self.p)

        elif self.classifier==2:
            self.block_classification=MultiScaleCNN_Classification_Deep_Dense(self.p)
        
        

    def call(self, input_tensor, training=False):

        x = self.permute(input_tensor)

        if self.p.parameters["previous_conv"]=='1':
            x=self.previous_conv(x)

        x_out_pb_list=[]
        for pb in self.block_list:
            x_out_pb_list.append(pb(x))

        if self.p.parameters["concat_mode"]=="ALL":
            x_concat_avg=self.concatBlock_avg(x_out_pb_list)
            x_concat_min=self.concatBlock_min(x_out_pb_list)
            x_concat_max=self.concatBlock_max(x_out_pb_list)
            x_concat=self.concatBlock([x_concat_min, x_concat_avg, x_concat_max])
        else:
            x_concat=self.concatBlock(x_out_pb_list)

        if self.p.parameters["attention"]=="1":
            x_concat=self.attention(x_concat)

        x=self.drop(x_concat, training=training) if self.training_dropout  > 0 else self.drop(x_concat)

        if self.classifier==0:
            x=self.block_classification(x)
            x=self.final_activation(x)
        else:
             x=self.block_classification(x)

        return x
    
    def _set_parameters(self, p):
        self.p=p

        self.output_model=1
        if "output" in p.parameters:
            self.output_model=int(p.parameters["output_model"])

        self.training_dropout=int(p.parameters["training_dropout"])

        self.classifier=0
        if "classifier" in p.parameters:
            self.classifier=int(p.parameters["classifier"])
        

        
            
    
    def model(self, shape=(19, 256)):
        x = tf.keras.Input(shape=shape)
        return tf.keras.Model(inputs=[x], outputs=self.call(x))

class MultiScale_Recurrent(tf.keras.Model):

    def __init__(self, p):
        super(MultiScale_Recurrent, self).__init__()

        self._init_params(p)

        self.permute = tf.keras.layers.Permute((2,1))

        self.conv=tf.keras.layers.Conv1D(
            self.filters_f1,
            self.kernel_size_f1
            ,padding='same')
        
        self.conv_norm=tf.keras.layers.BatchNormalization()
        
        self.dept_conv=tf.keras.layers.DepthwiseConv1D(
            self.depth_kernel_size,
            padding='same',
            depth_multiplier=self.depth_multiplier)

        self.depth_norm=tf.keras.layers.BatchNormalization()

        self.depth_activation=tf.keras.layers.Activation(self.depth_activation)

        self.depth_pool=tf.keras.layers.MaxPool1D(4)

        self.sep_conv=tf.keras.layers.SeparableConv1D(self.filters_conv, self.kernel_size_conv, padding='same')
        self.sep_conv_norm=tf.keras.layers.BatchNormalization()
        self.sep_conv_activation=tf.keras.layers.Activation(self.activation_conv)
        self.sep_conv_pool=tf.keras.layers.MaxPool1D(4)

        self.global_max_conv=tf.keras.layers.GlobalMaxPooling1D()
        self.flat=tf.keras.layers.Flatten()

        self.recurrent_01=tf.keras.layers.LSTM(self.units_lstm_f1, dropout=self.dropout_lstm, return_sequences=True)
        self.recurrent_01_norm=tf.keras.layers.BatchNormalization()
        self.recurrent_01_pool=tf.keras.layers.MaxPool1D()

        self.recurrent_02=tf.keras.layers.LSTM(self.units_lstm_f2, dropout=self.dropout_lstm)
        self.recurrent_02_norm=tf.keras.layers.BatchNormalization()

        self.concat=tf.keras.layers.Concatenate()


        self.block_classification_dense_01=tf.keras.layers.Dense(self.units_classification_dense_01)
        self.block_classification_activation_01=tf.keras.layers.Activation('relu')
        self.block_classification_dense_01_drop=tf.keras.layers.Dropout(0.5)

        self.block_classification_dense_02=tf.keras.layers.Dense(self.units_classification_dense_01//2)
        self.block_classification_activation_02=tf.keras.layers.Activation('relu')
        self.block_classification_dense_02_drop=tf.keras.layers.Dropout(0.5)

        self.block_classification_dense_03=tf.keras.layers.Dense(self.units_classification_dense_01//4)
        self.block_classification_activation_03=tf.keras.layers.Activation('relu')
        self.block_classification_dense_03_drop=tf.keras.layers.Dropout(0.5)

        self.block_classification_dense_04=tf.keras.layers.Dense(self.units_classification_dense_01//8)
        self.block_classification_activation_04=tf.keras.layers.Activation('relu')
        self.block_classification_dense_04_drop=tf.keras.layers.Dropout(0.5)

        self.block_classification_dense_out_norm=tf.keras.layers.BatchNormalization()
        self.block_classification_dense_out=tf.keras.layers.Dense(1)
        self.block_classification_activation_out=tf.keras.layers.Activation('sigmoid', name = 'sigmoid')

    def call(self, input_tensor, training=False):

        p=self.permute(input_tensor)

        x=self.conv(p)
        x=self.conv_norm(x, training=training)

        x=self.dept_conv(x)
        x=self.depth_norm(x, training=training)
        x=self.depth_activation(x)
        x=self.depth_pool(x)

        x=self.sep_conv(x)
        x=self.sep_conv_norm(x, training=training)
        x=self.sep_conv_activation(x)
        x=self.sep_conv_pool(x)

        x=self.global_max_conv(x)
        #x=self.flat(x)

        r=self.recurrent_01(p, training=training)
        r=self.recurrent_01_norm(r, training=training)
        r=self.recurrent_01_pool(r)

        r=self.recurrent_02(r, training=training)
        r=self.recurrent_02_norm(r, training=training)

        c=self.concat([x,r])

        c=self.block_classification_dense_01(c)
        c=self.block_classification_activation_01(c)
        c=self.block_classification_dense_01_drop(c)

        c=self.block_classification_dense_02(c)
        c=self.block_classification_activation_02(c)
        c=self.block_classification_dense_02_drop(c)

        c=self.block_classification_dense_03(c)
        c=self.block_classification_activation_03(c)
        c=self.block_classification_dense_03_drop(c)

        c=self.block_classification_dense_04(c)
        c=self.block_classification_activation_04(c)
        c=self.block_classification_dense_04_drop(c)

        c=self.block_classification_dense_out_norm(c, training=training)
        c=self.block_classification_dense_out(c)
        c=self.block_classification_activation_out(c)

        return c

    def model(self, shape=(19, 256)):
        x = tf.keras.Input(shape=shape)
        return tf.keras.Model(inputs=[x], outputs=self.call(x))
    
    def _init_params(self, p):

        self.p=p
        
        self.filters_f1=int(p.parameters["filters_f1"])
        self.kernel_size_f1=int(p.parameters["kernel_size_f1"])

        self.units_lstm_f1=int(p.parameters["units_lstm_f1"])
        self.dropout_lstm=float(p.parameters["dropout_lstm"])
        self.units_lstm_f2=int(p.parameters["units_lstm_f2"])

        self.depth_kernel_size=int(p.parameters["depth_kernel_size"])
        self.depth_multiplier=int(p.parameters["depth_multiplier"])
        self.depth_activation=p.parameters["depth_activation"]

        self.filters_conv=int(p.parameters["filters_conv"])
        self.kernel_size_conv=int(p.parameters["kernel_size_conv"])
        self.activation_conv=p.parameters["activation_conv"]

        self.units_classification_dense_01=int(p.parameters["units_classification_dense_01"])



class JSModel_Recurrent(tf.keras.Model):
    def __init__(self, p):
        
        super(JSModel_Recurrent, self).__init__()

        self.lstm_01=tf.keras.layers.LSTM(256, dropout=0.5, return_sequences=True)
        self.norm_lstm=tf.keras.layers.BatchNormalization()
        self.lstm_02=tf.keras.layers.LSTM(128, dropout=0.5)

        self.dense_01=tf.keras.layers.Dense(256, activation='relu')
        self.drop_01=tf.keras.layers.Dropout(0.5)

        self.dense_02=tf.keras.layers.Dense(128, activation='relu')
        self.drop_02=tf.keras.layers.Dropout(0.5)

        self.dense_03=tf.keras.layers.Dense(32, activation='relu')
        self.drop_03=tf.keras.layers.Dropout(0.5)

        self.classification=tf.keras.layers.Dense(1, activation='sigmoid')

    def call(self, input_tensor, training=False):

        x_lstm=self.lstm_01(input_tensor, training=training)
        x_lstm=self.norm_lstm(x_lstm, training=training)
        x_lstm=self.lstm_02(x_lstm, training=training)

        x=self.dense_01(x_lstm)
        x=self.drop_01(x, training=training)

        x=self.dense_02(x)
        x=self.drop_02(x, training=training)

        x=self.dense_03(x)
        x=self.drop_03(x, training=training)

        return self.classification(x)

class JSModel_MLP_03(tf.keras.Model):
    def __init__(self, p):
        super(JSModel_MLP_03, self).__init__()

        self.flatten=tf.keras.layers.Flatten()

        self.dense_01=tf.keras.layers.Dense(256, activation='relu')
        self.drop_01=tf.keras.layers.Dropout(0.5)

        self.dense_02=tf.keras.layers.Dense(128, activation='relu')
        self.drop_02=tf.keras.layers.Dropout(0.5)

        self.dense_03=tf.keras.layers.Dense(32, activation='relu')
        self.drop_03=tf.keras.layers.Dropout(0.5)

        self.classification=tf.keras.layers.Dense(1, activation='sigmoid')

    def call(self, input_tensor, training=False):
    
        x=self.flatten(input_tensor)

        x=self.dense_01(x)
        x=self.drop_01(x, training=training)

        x=self.dense_02(x)
        x=self.drop_02(x, training=training)

        x=self.dense_03(x)
        x=self.drop_03(x, training=training)

        return self.classification(x)


class EEGNet(tf.keras.Model):

    def __init__(self, p):
        super(EEGNet, self).__init__()

        #self.permute = tf.keras.layers.Permute((2,1))

        # Block 01
        self.block_01_conv2D=tf.keras.layers.Conv2D(
            int(p.parameters["F1"]), 
            (1, int(p.parameters["kernLength"])), 
            padding = 'same',
            use_bias = False)
        
        self.block_01_norm_01=tf.keras.layers.BatchNormalization()
        self.block_01_deptwise2D=tf.keras.layers.DepthwiseConv2D(
            (int(p.parameters["Chans"]), 1), 
            use_bias = False, 
            depth_multiplier = int(p.parameters["D"]),
            depthwise_constraint = tf.keras.constraints.max_norm(1.))
        
        self.block_01_norm_02=tf.keras.layers.BatchNormalization()
        self.block_01_activation_01=tf.keras.layers.Activation('elu')
        self.block_01_pool=tf.keras.layers.AveragePooling2D((1, 4))
        self.block_01_drop=tf.keras.layers.Dropout(float(p.parameters["dropoutRate"]))

        # Block 02

        self.block_02_sepConv2D=tf.keras.layers.SeparableConv2D(
            int(p.parameters["F2"]), 
            (1, 16),
            use_bias = False, 
            padding = 'same')
        self.block_02_norm_01=tf.keras.layers.BatchNormalization()
        self.block_02_activation_01=tf.keras.layers.Activation('elu')
        self.block_02_pool=tf.keras.layers.AveragePooling2D((1, 8))
        self.block_02_drop=tf.keras.layers.Dropout(float(p.parameters["dropoutRate"]))
        self.block_02_flat=tf.keras.layers.Flatten(name = 'flatten')


        # Classification

        self.block_classification_dense=tf.keras.layers.Dense(
            1, 
            kernel_constraint = tf.keras.constraints.max_norm(float(p.parameters["norm_rate"])))
        self.block_classification_activation=tf.keras.layers.Activation('sigmoid', name = 'sigmoid')
        
    def call(self, input_tensor, training=False):

        # input_tensor=self.permute(input_tensor)
        expanded_tensor=tf.expand_dims(input_tensor, -1, name='Adapting_2D')

        block1=self.block_01_conv2D(expanded_tensor)
        block1=self.block_01_norm_01(block1, training=training)
        block1=self.block_01_deptwise2D(block1)
        block1=self.block_01_norm_02(block1, training=training)
        block1=self.block_01_activation_01(block1)
        block1=self.block_01_pool(block1)
        block1=self.block_01_drop(block1)

        block2=self.block_02_sepConv2D(block1)
        block2=self.block_02_norm_01(block2, training=training)
        block2=self.block_02_activation_01(block2)
        block2=self.block_02_pool(block2)
        block2=self.block_02_drop(block2)
        
        flatten=self.block_02_flat(block2)
        
        dense=self.block_classification_dense(flatten)
        output=self.block_classification_activation(dense)

        return output
        
    def model(self, shape=(19, 256)):
        x = tf.keras.Input(shape=shape)
        return tf.keras.Model(inputs=[x], outputs=self.call(x))

class VGGNet(tf.keras.Model):
    def __init__(self, p):
        super(VGGNet, self).__init__()


        # input_main   = Input((height, width, depth))

        

        self.block1_01=tf.keras.layers.Conv2D(filters=64, kernel_size=3, padding='same', activation='relu')
        self.block1_02=tf.keras.layers.Conv2D(filters=64, kernel_size=3, padding='same', activation='relu')
        self.block1_03=tf.keras.layers.MaxPooling2D(pool_size=2, strides=2, padding='same')

        self.block2_01=tf.keras.layers.Conv2D(filters=128, kernel_size=3, padding='same', activation='relu')
        self.block2_02=tf.keras.layers.Conv2D(filters=128, kernel_size=3, padding='same', activation='relu')
        self.block2_03=tf.keras.layers.MaxPooling2D(pool_size=2, strides=2, padding='same')

        self.block3_01=tf.keras.layers.Conv2D(filters=256, kernel_size=3, padding='same', activation='relu')
        self.block3_02=tf.keras.layers.Conv2D(filters=256, kernel_size=3, padding='same', activation='relu')
        self.block3_03=tf.keras.layers.Conv2D(filters=256, kernel_size=3, padding='same', activation='relu')
        self.block3_04=tf.keras.layers.MaxPooling2D(pool_size=2, strides=2, padding='same')

        self.block4_01=tf.keras.layers.Conv2D(filters=512, kernel_size=3, padding='same', activation='relu')
        self.block4_02=tf.keras.layers.Conv2D(filters=512, kernel_size=3, padding='same', activation='relu')
        self.block4_03=tf.keras.layers.Conv2D(filters=512, kernel_size=3, padding='same', activation='relu')
        self.block4_04=tf.keras.layers.MaxPooling2D(pool_size=2, strides=2, padding='same')

        self.block5_01=tf.keras.layers.Conv2D(filters=512, kernel_size=3, padding='same', activation='relu')
        self.block5_02=tf.keras.layers.Conv2D(filters=512, kernel_size=3, padding='same', activation='relu')
        self.block5_03=tf.keras.layers.Conv2D(filters=512, kernel_size=3, padding='same', activation='relu')
        self.block5_04=tf.keras.layers.MaxPooling2D(pool_size=2, strides=2, padding='same')

        #Dense Layers
        self.block6_01=tf.keras.layers.Flatten()
        self.block6_02=tf.keras.layers.Dense(units=4096, activation='relu')
        self.block6_03=tf.keras.layers.Dropout(0.5)
        self.block6_04=tf.keras.layers.Dense(units=4096, activation='relu')
        self.block6_05=tf.keras.layers.Dropout(0.5)


        self.dense_out    = tf.keras.layers.Dense(units=1, name = 'outputdense_bin')
        self.modeloutput  = tf.keras.layers.Activation('sigmoid', name = 'sigmoid')

    def call(self, input_tensor, training=False):

        expanded_tensor=tf.expand_dims(input_tensor, -1, name='Adapting_2D')

        block1=self.block1_01(expanded_tensor)
        block1=self.block1_02(block1)
        block1=self.block1_03(block1)

        block2=self.block2_01(block1)
        block2=self.block2_02(block2)
        block2=self.block2_03(block2)

        block3=self.block3_01(block2)
        block3=self.block3_02(block3)
        block3=self.block3_03(block3)
        block3=self.block3_04(block3)

        block4=self.block4_01(block3)
        block4=self.block4_02(block4)
        block4=self.block4_03(block4)
        block4=self.block4_04(block4)

        block5=self.block5_01(block4)
        block5=self.block5_02(block5)
        block5=self.block5_03(block5)
        block5=self.block5_04(block5)

        block6=self.block6_01(block5)
        block6=self.block6_02(block6, training=training)
        block6=self.block6_03(block6)
        block6=self.block6_04(block6, training=training)
        block6=self.block6_05(block6)

        x=self.dense_out(block6)    
        return self.modeloutput(x)
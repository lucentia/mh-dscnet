
from tensorflow import keras

import numpy as np


class BestAtMinLossWithAccThreshold(keras.callbacks.Callback):
    """Stop training when the loss is at its min, i.e. the loss stops decreasing.

    Arguments:
        patience: Number of epochs to wait after min has been hit. After this
        number of no improvement, training stops.
    """

    def __init__(self, jsLog, binary_accuracy_threshold=0.95, patience=5):
        super().__init__()
        self.binary_accuracy_threshold=binary_accuracy_threshold
        self.patience = patience
        # best_weights to store the weights at which the minimum loss occurs.
        self.best_weights = None
        self.jsLog=jsLog

    def on_train_begin(self, logs=None):
        # The number of epoch it has waited when loss is no longer minimum.
        self.wait = 0
        # The epoch the training stops at.
        self.stopped_epoch = 0
        # Initialize the best as infinity.
        self.best_val_loss = np.Inf
        self.best_binary_accuracy = np.Inf

    def on_epoch_end(self, epoch, logs=None):
        current_binary_accuracy = logs.get("binary_accuracy")
        current_val_loss = logs.get("val_loss")
        # Start recording when binary_accuracy > 0.95
        self.jsLog.w(f"CALLBACK ON_EPOCH_END: [{current_binary_accuracy}][{current_val_loss}] - [{self.best_binary_accuracy}][{self.best_val_loss}]", True)
        if current_binary_accuracy > self.binary_accuracy_threshold and np.less(current_val_loss, self.best_val_loss):
            self.best_val_loss = current_val_loss
            self.best_binary_accuracy = logs.get("binary_accuracy")
            self.wait = 0
            # Record the best weights if current results is better (less).
            self.best_weights = self.model.get_weights()
            self.jsLog.w(f"SAVED WEIGHTS: [{current_binary_accuracy}][{current_val_loss}] - [{self.best_binary_accuracy}][{self.best_val_loss}]", True)

        # else:
        #     self.wait += 1
        #     if self.wait >= self.patience:
        #         self.stopped_epoch = epoch
        #         self.model.stop_training = True
        #         print("Restoring model weights from the end of the best epoch.")
        #         self.model.set_weights(self.best_weights)

    def on_train_end(self, logs=None):
        if self.stopped_epoch > 0:
            self.jsLog.w("Epoch %05d: early stopping" % (self.stopped_epoch + 1), True)
            self.model.set_weights(self.best_weights)
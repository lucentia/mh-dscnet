# Multiscale Deep Learning Convolutional Neural Network for ADHD Detection using EEG


## Description
Source code Multiscale Deep Learning Convolutional Neural Network for ADHD Detection using EEG

## Installation

### Clone or Download the Repository

You can clone or download the source code from our GitLab repository in one of the following ways:

- **Clone via SSH:**
git clone git@gitlab.com:lucentia/mh-dscnet.git


- **Download Zip Archive:**
[Download from Lucentia GitLab Repository](https://gitlab.com/lucentia/mh-dscnet)

### EEG Dataset Download and Extraction

#### Step 1: Download the EEG Dataset
- Download the EEG Dataset from [IEEEDataPort - EEG DATA FOR ADHD / CONTROL CHILDREN](https://ieee-dataport.org/open-access/eeg-data-adhd-control-children).

#### Step 2: Create a Local Data Folder
- Create a folder on your local storage. For example, name it "mh_dscnet_data."

#### Step 3: Organize Subfolders
Inside the "mh_dscnet_data" folder, create two subfolders:
- "mh_dscnet_data/ADHD"
- "mh_dscnet_data/CONTROLS"

#### Step 4: Extract and Copy Files
##### For ADHD Data
- Unzip "ADHD_part1.zip" and "ADHD_part2.zip."
- Copy all the extracted files into the "mh_dscnet_data/ADHD" folder.

##### For Control Data
- Unzip "Control_part1.zip" and "Control_part2.zip."
- Copy all the extracted files into the "mh_dscnet_data/CONTROLS" folder.

#### Step 5: Create a directory for the logs
In our case we created the folder "logs/mh-dscnet"

### Install and Run TensorFlow Docker

To install TensorFlow in a Docker container, please follow the official [TensorFlow Docker installation guide](https://www.tensorflow.org/install/docker).

Once you've installed Docker and TensorFlow, you can run TensorFlow with GPU support using the following command:

```bash
docker run --gpus all -it --name mh-dscnet -v /home/javier/Documents/Doctorat/mh-dscnet/:/code -v /home/javier/Documents/Doctorat/data/mh_dscnet_data/:/data -v /home/javier/Documents/Doctorat/logs/mh-dscnet/:/logs -w /code tensorflow/tensorflow:latest-gpu bash
```

As can be seen in the command, we have mapped the "code", "data", and "logs" folders to their respective local directories.

### Install Python Requirements

First, install the required Python libraries using pip:

```bash
pip install -r requirements.txt
```

This command will install the necessary Python libraries needed to run the experiments.

## Usage

To run the experiments in the Docker container, use the following command in the terminal:

```bash
python JSLaunchExperiment.py -i 10 -m MultiScaleCNN
```

Here, the parameter "m" indicates the model, which in this case is "MultiScaleCNN" for the "mh-dscnet." The parameter "i" refers to the hyperparameter configuration, which is loaded from "conf/MultiScaleCNN.csv" with the configuration ID 10.

In order to run the experiments made for comparision

```bash
python JSLaunchExperiment.py -i 2 -m JSModel_MLP_03
python JSLaunchExperiment.py -i 2 -m EEGNet
python JSLaunchExperiment.py -i 2 -m JSModel_Recurrent
python JSLaunchExperiment.py -i 2 -m VGGNet
```

## Support

If you have any questions or require assistance, please feel free to contact us via email:

- Javier Sanchis: [javier.sanchis@ua.es](mailto:javier.sanchis@ua.es)

## Authors and Acknowledgments

We extend our gratitude to the following authors and contributors for their valuable contributions to this project:

- Prof. Tahar Kechadi: [tahar.kechadi@ucd.ie](mailto:tahar.kechadi@ucd.ie)
- PhD. Miguel A. Teruel: [materuel@dlsi.ua.es](mailto:materuel@dlsi.ua.es)
- Prof. Juan Trujillo: [jtrujillo@dlsi.ua.es](mailto:jtrujillo@dlsi.ua.es)

This work has been co-funded by the  AETHER-UA project (PID2020-112540RB-C43), a smart data holistic
approach  for context-aware  data analytics:  smarter machine  learning for  business modelling  and
analytics, funded by Spanish Ministry of Science and Innovation; The BALLADEER (PROMETEO/ 2021/ 088)
project,  a Big  Data analytical  platform  for the  diagnosis  and treatment  of Attention  Deficit
Hyperactivity Disorder (ADHD) featuring extended reality, funded by the Conselleria de Innovaci\'on,
Universidades, Ciencia  y Sociedad Digital  (Generalitat Valenciana);  Program for the  Promotion of
R+D+I  (UAIND20-03B);  Vicerrectorado  de  Investigación  y  Transferencia  de  Conocimiento  de  la
Universidad de Alicante. This  work is also supported by Science  Foundation Ireland (12/RC/2289 P2)
at Insight the SFI Research Centre for Data Analytics at University College Dublin.

## License

## Project status

